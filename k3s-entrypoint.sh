#!/bin/sh

echo "Running on hostname "$(hostname)

echo "Starting docker"
/usr/local/bin/dockerd-entrypoint.sh &

echo "Waiting for docker to appear"
i=0
while [ ! -e /var/run/docker.sock ]
do
        i=$(($i+1))
        if [ $i -gt 120 ]
        then
                echo "Docker did not appear in 120 seconds..." 
                exit -1
        fi
        echo "Still Waiting for docker to appear"
        sleep 1
done

echo "Starting k3s"
/usr/bin/k3s agent --node-label nodetype="${K3S_NODETYPE}" --node-label smarter/type=edge --docker --no-flannel --token "${K3S_TOKEN}" --server "${K3S_SERVER}" --node-name ${K3S_NODENAME} --kubelet-arg cluster-dns=${K3S_CLUSTER_DNS} --resolv-conf /etc/resolv.conf
