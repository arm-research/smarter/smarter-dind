FROM docker:dind

COPY daemon.json /etc/docker/
COPY k3s-entrypoint.sh /usr/local/bin
RUN cd /usr/bin;\
    if [ $(uname -m) == "x86_64" ];then wget 'https://gitlab.com/arm-research/smarter/k3s/-/jobs/702137534/artifacts/raw/v1.18.3-k3s2-smarter/amd64/k3s';fi;\
    if [ $(uname -m) == "aarch64" ];then wget 'https://gitlab.com/arm-research/smarter/k3s/-/jobs/702137534/artifacts/raw/v1.18.3-k3s2-smarter/arm64/k3s-arm64';mv k3s-arm64 k3s;fi;\
    chmod u+x k3s /usr/local/bin/k3s-entrypoint.sh

VOLUME /var/lib/kubelet
VOLUME /var/lib/rancher/k3s
VOLUME /var/lib/cni
VOLUME /var/log

ENTRYPOINT ["/usr/local/bin/k3s-entrypoint.sh"]
CMD []
